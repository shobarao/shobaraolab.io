---

This project documents the design specifications associated with the [Phoenix project](https://gitlab.com/VisteonCorp/chromium-49).
It is an attempt at putting together a template that can be used by other application developers within the organization.

In devising this template, I have gleaned information from w3c sites such as [Payment Request APi](https://w3c.github.io/browser-payment-api/#introduction), 
[Web IDL](https://heycam.github.io/webidl/) and the software architects on the team. 
We've made every effort *not* to assume or impose a particular methodology or paradigm, and to place more emphasis on content than on format.
---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Goal](#goal)
- [Document Outline](#document-outline)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Goal 

The goal of this specification document is fairly simple and aims to:
  * serve as an adequate training material for new project members, imparting to them enough information and understanding about the project implementation to create or modify source code.
  * be as detailed as possible, while at the same time not imposing too much of a burden on the designers and/or implementors that it becomes overly difficult to understand, create or maintain.

## Document Outline

Here is an outline for the proposed design specification document. Please note that many parts of this document may be a part of other project documents.
What follows is a suggestion that attempts to capture the design of the functional area as a single document. This by no means implies that it literally is a single document.

1. Introduction
  1. Goals
  2. Non-Goals
2. Conformance
3. Dependencies
4. *InterfaceName* 
  1. Attributes 
  2. Extended Attributes
  3. Methods
  4. Constructors
5. Working with the interface/API 
  1. Loading the API
  2. Creating a Request
  3. ....
6. Gotchas
